CC?=icc

.PHONY: all
all: hello.exe hello_lib.exe hello_dlopen.exe libhello.so

hello.exe: hello_omp.c
	$(CC) -g -fopenmp hello_omp.c -o hello.exe
hello_lib.exe: main_link.c libhello.so
	$(CC) -g main_link.c -o hello_lib.exe -L. -lhello
hello_dlopen.exe: main_dlopen.c libhello.so
	$(CC) -g main_dlopen.c -o hello_dlopen.exe -ldl
libhello.so: hello_lib.c
	$(CC) -g -fopenmp -shared hello_lib.c -fPIC -o libhello.so

clean:
	rm hello.exe hello_lib.exe hello_dlopen.exe libhello.so