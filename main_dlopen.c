#include <omp.h>
#include <stdio.h>
#include <dlfcn.h>
#define THREADS 4


int main(int argc, char** argv){

  void* h = dlopen("libhello.so", RTLD_LAZY);
  void (*test)() = dlsym(h, "hello");
  test();
}
