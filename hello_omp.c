#include <omp.h>
#include <stdio.h>
#define THREADS 4

int main(int argc, char** argv){


#pragma omp parallel num_threads(THREADS)
{
        printf("Hello from Thread %i of %i\n", omp_get_thread_num(), omp_get_num_threads());
}

}