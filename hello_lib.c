#include <omp.h>
#include <stdio.h>
//#include "callback.h"
#define THREADS 4

void hello(){


#pragma omp parallel num_threads(THREADS)
{
        printf("Hello from Thread %i of %i\n", omp_get_thread_num(), omp_get_num_threads());
}

}